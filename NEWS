4.19.2 (2023-04-17)
======
- Update copyright year
- XfwScreenX11: Fix typo in property name when creating window
- Use XDT_CHECK_PACKAGE_BINARY for wayland-scanner
- Use XDT_CHECK_OPTIONAL_FEATURE
- Use XDT_VERSION_INIT() and eliminate need for configure.ac.in
- Use $PKG_CONFIG and not pkg-config in configure.ac
- autogen.sh: fix xfce4-dev-tools dependency version
- build: Get rid of IntlTool
- Use bind_textdomain_codeset() if available
- build: Fix autotools warning
- Update bug report address
- build: Fix "make dist" when disable wayland
- configure: Fix X11's dependencies detection
- XfwWnckIcon: Silently return NULL if no X11 window can be found
- Add new CI builds to build without X11 and Wayland
- configure: error out if no windowing backend enabled
- Add configure args to enable/disable X11/Wayland
- Fix Wayland-only build
- Translation Updates:
  Albanian, Bulgarian, Chinese (China), Chinese (Taiwan), Croatian,
  Dutch, English (Canada), English (United Kingdom), Finnish, French,
  German, Hebrew, Indonesian, Interlingue, Italian, Japanese, Korean,
  Lithuanian, Norwegian Bokmål, Occitan (post 1500), Polish,
  Portuguese, Portuguese (Brazil), Romanian, Russian, Serbian,
  Slovenian, Spanish, Swedish, Turkish, Ukrainian

4.19.1 (2023-01-09)
======
- Allow callers to determine if a returned icon is a fallback icon
- Remove duplicate _get_icon() code
- Return correct fallback icon for XfwWindowWayland
- Cache GIcon and GdkPixbuf in XfwApplication
- Cache GIcon and GdkPixbuf in XfwWindow
- Add xfw_(window|application)_get_gicon()
- XfwApplication: Make XfwApplicationInstance opaque
- Fix build when X11 disabled


4.19.0 (2023-01-02)
======
- Initial release
