Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libxfce4windowing
Source: https://gitlab.xfce.org/xfce/libxfce4windowing

Files: *
Copyright: 2022, Gaël Bonithon <gael@xfce.org>
           2022-2023, Brian Tarricone <brian@tarricone.org>
License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".

Files: protocols/*
Copyright: 2019, Christopher Billington
           2018-2020, Ilia Bozhinov
           2022, Victoria Brekenfeld
License: Expat

Files: debian/*
Copyright: 2022-2023, Unit 193 <unit193@debian.org>
License: Expat

License: Expat
 Permission to use, copy, modify, distribute, and sell this
 software and its documentation for any purpose is hereby granted
 without fee, provided that the above copyright notice appear in
 all copies and that both that copyright notice and this permission
 notice appear in supporting documentation, and that the name of
 the copyright holders not be used in advertising or publicity
 pertaining to distribution of the software without specific,
 written prior permission.  The copyright holders make no
 representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied
 warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 THIS SOFTWARE.
